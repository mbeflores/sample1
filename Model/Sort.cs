﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouSource.Model
{
    public class Sort
    {
        public string Message { get; set; }
        public string Strategy { get; set; }
        public string Result { get; set; }
    }
}
