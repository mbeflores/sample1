﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YouSource.Controller;
using YouSource.Model;
namespace YouSource.Model
{
    public interface ISortView
    {
        void SetController(SortController controller);    
        string Input { get; set; }
        string Strategy { get; set; }
        string Result{ get; set; }
    }
}
