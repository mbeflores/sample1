﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using YouSource.Controller;
using YouSource.Model;

namespace YouSource
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            SortView view= new SortView();
            Sort sort = new Sort();
            view.Visible = false;
            SortController controller = new SortController(view,sort);
            view.ShowDialog();
        }
    }
}
