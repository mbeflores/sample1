﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YouSource.Controller;
using YouSource.Model;

namespace YouSource
{
    public partial class SortView : Form,ISortView
    {
        private SortController _sortController;
        public SortView()
        {
            InitializeComponent();
        }

      


        private void button1_Click(object sender, EventArgs e)
        {
            _sortController.Sort();

        }

        


        public void SetController(SortController controller)
        {
            _sortController = controller;
        }

        private string _input;
        private string _strategy;
        private string _result;

        public string Input
        {
            get=>this.txtInput.Text;
            set=>this.txtInput.Text= value;
        }

        public string Strategy
        {
            get=>cmbStrategy.SelectedItem.ToString();
            set=>cmbStrategy.SelectedItem=value;
        }

        public string Result
        {
            get=>lblResult.Text;
            set=>lblResult.Text=value;
        }
    }
}
