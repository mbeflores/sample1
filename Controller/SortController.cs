﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouSource.Model;

namespace YouSource.Controller
{
    public class SortController
    {
        private ISortView _sortView;
        private Sort _sort;

        public SortController(ISortView sortView, Sort sort)
        {
            _sortView = sortView;
            _sort = sort;
            sortView.SetController(this);
        }

        public void Sort()
        {
            var strategy = _sortView.Strategy;
            var input = _sortView.Input;
            
            if (strategy == "Bubble Sort")
            {
                var result = BubbleSort(input);
                _sortView.Result = result;
            }
            else if (strategy == "Original State")
            {
                _sortView.Result = input;
            }

        }

        

        static string BubbleSort(string input)
        {
            int upperLimit = input.Length;
            List<char> chars = new List<char>(input.ToCharArray());
            for (int i = 0; i < upperLimit - 1; i++)
            {
                for (int j = 0; j < upperLimit - 1; j++)
                {
                    if (chars[j] > chars[j + 1])
                    {
                        char temp = chars[j];
                        chars[j] = chars[j + 1];
                        chars[j + 1] = temp;
                    }
                }
            }

            return string.Join(", ", chars);
        }
    }
}
